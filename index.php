<html lang="en">
    
    <head>
        <meta charset="utf-8">
        <title>Working with json configuration</title>
    </head>

    <body>
        <?php

        //Import class
        require_once './ConfigurationFile.php';

        //Set namespace
        use m4taiori\JsonConfig\ConfigurationFile as ConfigurationFile;

        //The defaults.
        $Defaults = 
        [
            "Hello" => "World",
            "AnArray" => [ 
                            "Item1" => "Value1",
                            "Item2" => "Value2"
                         ]
        ];

        //The configuration file.
        $Config = new ConfigurationFile( './config.json', $Defaults );

        //Echo the test value!
        echo 'Our test-value: ';
        echo $Config->get( 'Hello' );

        echo '<br>' . '<br>';

        echo 'Our saved assoc: ' . '<br>';
        echo var_dump( $Config->get( 'AnArray' ) );

        ?>
        
        <br>
        <br>
        Learn how it works: <a href="doc/index.html" title="See documentation" target="_blank">See documentation</a> !
        
    </body>

</html>