<?php

/*
 * Copyright (C) 2017 Jens F.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace m4taiori\JsonConfig;

/**
 * A simple json based configuration.
 *
 * @version 1.0.0
 * @author Jens F.
 */
class ConfigurationFile 
{
    
    /** @var String The config files path. */
    private $File;
    
    /** @var Assoc The config content as assoc-array. */
    private $Content = [];
    
    /** @var Boolean Is auto-flush enabled. */
    private $AutoFlush = TRUE;
    
    /**
     * Construction
     * 
     * @param String $file The config-files path.
     * @param Array $defaults An array including all default config values. 
     */
    function __construct( $file, $defaults = null ) 
    {
        
        $this->File = $file;
        
        $this->read();
        
        if ( $defaults != NULL )
        {
            $this->loadDefaults( $defaults );
        }
        
    }
    
    /**
     * Set a config value.
     * Flushing if enabled.
     * 
     * @param String $key The key you wanna set.
     * @param ? $value It's value.
     */
    public function set( $key, $value )
    {
        
        $this->Content[ $key ] = $value;
        
        if ( $this->AutoFlush )
        {
            $this->flush();
        }
        
    }
        
    /**
     * Get a config value.
     * 
     * @param String $key The key you're looking for.
     * @return object 
     */
    public function get( $key )
    {
        if ( isset( $this->Content[ $key ] ) )
        {
            return $this->Content[ $key ];
        }
        else
        {
            return NULL;
        }
    }
    
    /**
     * Read config to runtime.
     */
    private function read()
    {
        if ( file_exists( $this->File ) )
        {
            $Values = json_decode( file_get_contents( $this->File ), TRUE );
            
            $this->Content = $Values;
        }
    }
    
    /**
     * Save values from runtime to file.
     */
    public function flush()
    {
        
        $Values = json_encode( $this->Content, JSON_PRETTY_PRINT );
        
        file_put_contents( $this->File , $Values );
        
    }
    
    /**
     * Enable/Disable auto-flushing.
     * True by default.
     * 
     * @param Boolean $state Option state.
     */
    public function autoFlush( $state = TRUE )
    {
        $this->AutoFlush = $state;
    }
    
    /**
     * Load the defaults to config.
     * 
     * @param Array $defaults An array including all default config value.
     */
    private function loadDefaults( $defaults )
    {
        foreach ( array_keys( $defaults ) as $Key )
        {
            if ( $this->get( $Key ) == NULL )
            {
                $this->set( $Key , $defaults[ $Key ] );
            }
        }
        $this->flush();
    }
    
}
